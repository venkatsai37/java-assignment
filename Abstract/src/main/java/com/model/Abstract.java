package com.model;

public abstract class Abstract {

	private int empno;
	private String empName;

	public Abstract() {
		super();
	}
	

	public Abstract(int empno, String empName) {
		super();
		this.empno = empno;
		this.empName = empName;
	}
   
	public abstract void run();

	public int getEmpno() {
		return empno;
	}

	public void setEmpno(int empno) {
		this.empno = empno;
	}

	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

	@Override
	public String toString() {
		return "Abstract [empno=" + empno + ", empName=" + empName + "]";
	}

}
