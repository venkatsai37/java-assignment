package com.service;

public class CalculatorServiceImpl implements CalculatorService {

	@Override
	public int ADD(int a, int b) {
		int c = a + b;
		System.out.println("The addition of a,b is:"+c);
		return c;
	}

	@Override
	public int SUB(int a, int b) {
		int c = a - b;
		System.out.println("The Sub of a,b is:"+c);
		return c;
	}

	@Override
	public int MUL(int a, int b) {
		int c = a * b;
		System.out.println("The mul of a,b is:"+c);
		return c;
	}

	@Override
	public int DIV(int a, int b) {
		int c = a % b;
		System.out.println("The division of a,b is:"+c);
		return c;
	}

	@Override
	public double sin(double number) {
		double radians = Math.toRadians(number);
		double valueOfSin = Math.sin(radians);
		System.out.println("The value of tan is:"+valueOfSin);
		return valueOfSin;
	}

	@Override
	public double cos(double number) {
		double radians = Math.toRadians(number);
		double valueOfCos = Math.cos(radians);
		System.out.println("The value of tan is:"+valueOfCos);
		return valueOfCos;
	}

	@Override
	public double tan(double number) {
		double radians = Math.toRadians(number);
		double valueOfTan = Math.tan(radians);
		System.out.println("The value of tan is:"+valueOfTan);
		return valueOfTan;
	}
	
	

}
