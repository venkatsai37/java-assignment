package com.main;

import java.util.Scanner;

import com.model.Address;
import com.service.AddressService;
/**
 * 
 * @author ks.venkatsai
 *
 */
public class AddressArray {

	public static void main(String[] args) {
		Address address1 = new Address(1, "chittoor", "AP", 517001);
		Address address2 = new Address(2, "Chennai", "TN", 517003);
		Address address3 = new Address(3, "Bangalore", "KA", 517005);
		Address address4 = new Address(4, "Thrivendram", "KE", 517009);

		Address[] addresses = new Address[4];
		addresses[0] = address1;
		addresses[1] = address2;
		addresses[2] = address3;
		addresses[3] = address4;

		AddressService addressService = new AddressService();
		Scanner scan = new Scanner(System.in);
		System.out.println("Enter Doorno to search");
		int sc = scan.nextInt();
		Address address = addressService.searchByDoorno(addresses, sc);
		if (address != null) {
			System.out.println("Address number:" + address.getDoorno());
			System.out.println("State name:" + address.getState());
		} else {
			System.out.println("The mentioned doorno not associated with anyother records");
		}
		System.out.println("Enter Zipcode to search");
		int ans = scan.nextInt();
		Address address5 = addressService.searchByZipcode(addresses, ans);
		if (address5 != null) {
			System.out.println("Add Doorno:" + address5.getDoorno());
		}
		else {
			System.out.println("The mentioned Zipcode not associated with anyother records");
		}
		scan.close();
	}

}
