package com.main;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

public class Java8Date {

	public static void main(String[] args) {
		System.out.println(LocalDate.now());
		System.out.println(LocalDateTime.now());
		System.out.println(LocalDateTime.now().getDayOfMonth());
		System.out.println(LocalDate.now().getYear());
		
	LocalDate date = LocalDate.of(1999, 02, 10);
	LocalDate date2 = LocalDate.now();
	System.out.println(ChronoUnit.DAYS.between(date, date2));
	}

}
