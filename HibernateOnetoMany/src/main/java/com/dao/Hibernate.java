package com.dao;


import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

import com.model.Department;
import com.model.Employee;



public class Hibernate {
	public static void main(String[] args) {
		StandardServiceRegistry ssr = new StandardServiceRegistryBuilder().configure("hibernate.xml").build();
		Metadata meta = new MetadataSources(ssr).getMetadataBuilder().build();
		SessionFactory factory = meta.getSessionFactoryBuilder().build();
		Session session = factory.openSession();
		Transaction transaction = session.beginTransaction(); // commit & rollback
		Department department = new Department();
		department.setDeptName("ComputerScience");
		
		Employee employee = new Employee("Venkat", "venkatsai37@gmail.com", department);
		session.save(employee);
		
		Employee employee2 = new Employee("Sai", "ksvenkatsai@gmail.com", department);
		session.save(employee2);
		
		List<Employee> employees = new ArrayList<Employee>();
		employees.add(employee);
		employees.add(employee2);
		department.setEmployees(employees);
		session.save(department);
		
		transaction.commit();
		
		System.out.println("successfully saved");

		session.close();
		factory.close();
	}
}
