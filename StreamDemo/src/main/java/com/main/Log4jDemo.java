package com.main;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Log4jDemo {
	private static final Logger LOGGER = LogManager.getLogger(Log4jDemo.class);

	public static void main(String[] args) {
		LOGGER.info("Welcome to logging");
		System.out.println("End");
	}

}
