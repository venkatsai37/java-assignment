package com.main;

import java.util.ArrayList;
import java.util.List;

public class StreamDemo {

	public static void main(String[] args) {
		List<String> list = new ArrayList<String>();
		list.add("Venkat");
		list.add("Sai");
		list.add("Saisree");
		list.add("Rushi");
		
		Long long1 = list.stream().filter((arg)->arg.length()>5).count();
		System.out.println(long1);
	}

}
