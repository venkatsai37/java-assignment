package com.service;
/**
 * This is to demonstrate Functional programming
 * @author ks.venkatsai
 *
 */
@FunctionalInterface
public interface SingleAbsInf {
	public abstract void run();
	
	public default void defaultMethod() {
		System.out.println("Its a default Method");
	}
	
	public static void staticMethod() {
		System.out.println("Its a static method");
	}
}
