package com.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

import com.model.Employee;

/**
 * This is to demonstrate how to display the result
 * 
 * @author ks.venkatsai
 *
 */
public class Hibernate3 {
	public static void main(String[] args) {
		StandardServiceRegistry ssr = new StandardServiceRegistryBuilder().configure("hibernate.xml").build();
		Metadata meta = new MetadataSources(ssr).getMetadataBuilder().build();
		SessionFactory factory = meta.getSessionFactoryBuilder().build();
		Session session = factory.openSession();
		String hsql = "from Employee";
		Query<Employee> query = session.createQuery(hsql);
		List<Employee> employees = query.list();
		for (Employee employee : employees) {
			System.out.println("Empno: " + employee.getEmpNo());
			System.out.println("Empname: " + employee.getEmpName());
		}
	}
}
