package com.dao;


import java.util.List;

import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;


import com.model.Student;

public class HibernateNamedQ {
	public static void main(String[] args) {
		StandardServiceRegistry ssr = new StandardServiceRegistryBuilder().configure("hibernate.xml").build();
		Metadata meta = new MetadataSources(ssr).getMetadataBuilder().build();
		SessionFactory factory = meta.getSessionFactoryBuilder().build();
		Session session = factory.openSession();
		Query query = session.getNamedQuery("findStudentFByName");
		query.setParameter("fname", "Venkat");
		List<Student> students = query.getResultList();
		for (Student student : students) {
			System.out.println(student.getStudId());
			System.out.println(student.getEmailId());
		}
		System.out.println("Reading by firstname");

		session.close();
		factory.close();
	}
}
