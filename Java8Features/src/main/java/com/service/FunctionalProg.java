package com.service;

import java.util.function.Function;

public class FunctionalProg {
	public String function(Function<Integer, String> fun) {
		fun = (a) -> {
			return "Welcom" +a;
		};
		String string = fun.apply(10);
		return "Happy" + string;
	}
}
