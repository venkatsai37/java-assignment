package com.main;

import java.util.function.BiConsumer;

public class Consumer {

	public static void main(String[] args) {
	BiConsumer<Integer, Integer> biConsumer = (a,b) ->{
		System.out.println("a value :"+a);
		System.out.println("b value :"+b);
		System.out.println("Addition of a,b: "+(a+b));
	};
	biConsumer.accept(1, 2);

	}

}
