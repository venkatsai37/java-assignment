package com.model;

public class Calculator {
	private int add;
	private int sub;
	private int mul;
	private int div;
	private double sin;
	private double cos;
	private double tan;
	public Calculator() {
		super();
	}
	public Calculator(int add, int sub, int mul, int div, double sin, double cos, double tan) {
		super();
		this.add = add;
		this.sub = sub;
		this.mul = mul;
		this.div = div;
		this.sin = sin;
		this.cos = cos;
		this.tan = tan;
	}
	public int getAdd() {
		return add;
	}
	public void setAdd(int add) {
		this.add = add;
	}
	public int getSub() {
		return sub;
	}
	public void setSub(int sub) {
		this.sub = sub;
	}
	public int getMul() {
		return mul;
	}
	public void setMul(int mul) {
		this.mul = mul;
	}
	public int getDiv() {
		return div;
	}
	public void setDiv(int div) {
		this.div = div;
	}
	public double getSin() {
		return sin;
	}
	public void setSin(double sin) {
		this.sin = sin;
	}
	public double getCos() {
		return cos;
	}
	public void setCos(double cos) {
		this.cos = cos;
	}
	public double getTan() {
		return tan;
	}
	public void setTan(double tan) {
		this.tan = tan;
	}
	
	
	
}
