package com.model;

public class Employee {
	private int empNO;
	private String empName;
	private float empSalary;
	private int empAge;
	
	private Address[] addresses;
	
	public Address[] getAddresses() {
		return addresses;
	}

	public void setAddresses(Address[] addresses) {
		this.addresses = addresses;
	}

	public Employee() {
		super();
	}

	public Employee(int empNO, String empName, float empSalary, int empAge) {
		super();
		this.empNO = empNO;
		this.empName = empName;
		this.empSalary = empSalary;
		this.empAge = empAge;
	}

	public int getEmpNO() {
		return empNO;
	}

	public void setEmpNO(int empNO) {
		this.empNO = empNO;
	}

	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

	public float getEmpSalary() {
		return empSalary;
	}

	public void setEmpSalary(float empSalary) {
		this.empSalary = empSalary;
	}

	public int getEmpAge() {
		return empAge;
	}

	public void setEmpAge(int empAge) {
		this.empAge = empAge;
	}
	
	
	
}
