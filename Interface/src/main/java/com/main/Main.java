package com.main;

import java.util.Scanner;

import com.service.CalculatorService;
import com.service.CalculatorServiceImpl;
/**
 * 
 * @author ks.venkatsai
 *
 */
public class Main {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		

		System.out.println("1 to perform Addition");
		System.out.println("2 to perform Subtraction");
		System.out.println("3 to perform Multiplication");
		System.out.println("4 to perform Division");
		System.out.println("5 to perform Sin");
		System.out.println("6 to perform Cos");
		System.out.println("7 to perform Tan");
		
		System.out.println("Enter a value for calculation");

		int num1, num2;
		double num3;

		CalculatorService calculatorService = new CalculatorServiceImpl();
		int fetch = scan.nextInt();

		System.out.println("Enter values followed by space for calculation");
	

		switch (fetch) {
		case 1:
			num1 = scan.nextInt();
			num2 = scan.nextInt();
			calculatorService.ADD(num1, num2);
			break;

		case 2:
			num1 = scan.nextInt();
			num2 = scan.nextInt();
			calculatorService.SUB(num1, num2);
			break;

		case 3:
			num1 = scan.nextInt();
			num2 = scan.nextInt();
			calculatorService.MUL(num1, num2);
			break;
		case 4:
			num1 = scan.nextInt();
			num2 = scan.nextInt();
			calculatorService.DIV(num1, num2);
			break;
		case 5:
			num3 = scan.nextDouble();
			calculatorService.sin(num3);
			break;
		case 6:
			num3 = scan.nextDouble();
			calculatorService.cos(num3);
			break;
		case 7:
			num3 = scan.nextDouble();
			calculatorService.tan(num3);
			break;
		default:
			break;

		}
		/*
		 * int a = scan.nextInt(); System.out.println("Enter b value"); int b =
		 * scan.nextInt(); CalculatorService calculatorService = new
		 * CalculatorServiceImpl(); calculatorService.ADD(a, b);
		 * System.out.println("Enter a value"); int c = scan.nextInt();
		 * System.out.println("Enter b value"); int d = scan.nextInt();
		 * calculatorService.SUB(c, d); System.out.println("Enter a value"); int e =
		 * scan.nextInt(); System.out.println("Enter b value"); int f = scan.nextInt();
		 * calculatorService.MUL(e, f); System.out.println("Enter a value"); int g =
		 * scan.nextInt(); System.out.println("Enter b value"); int h = scan.nextInt();
		 * calculatorService.DIV(g, h); System.out.println("Enter the value "); double
		 * fetch = scan.nextDouble(); DecimalFormat formater = new
		 * DecimalFormat("#.##"); String formated = formater.format(fetch);
		 * System.out.println("Sin Value of " + fetch + "=" +
		 * calculatorService.sin(fetch)); System.out.println("Cos Value of " + fetch +
		 * "=" + calculatorService.cos(fetch)); System.out.println("Tan Value of " +
		 * fetch + "=" + calculatorService.tan(fetch));
		 */
		scan.close();

	}

}
