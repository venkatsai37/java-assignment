package com.main;

import java.util.function.BiFunction;

import com.model.Employee;

public class BiFucDemo {

	public static void main(String[] args) {
		BiFunction<Integer, String, Employee> biFunction = (a,b) ->{
			return new Employee(a, b);
		};
		Employee employee =biFunction.apply(1, "venkat");
		System.out.println(employee.getEmpId());
		System.out.println(employee.getEmpName());
	}

}
