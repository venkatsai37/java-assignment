package com.main;

import java.util.ArrayList;
import java.util.List;

import com.model.Employee;

public class StreamUsingClass {

	public static void main(String[] args) {
		List<Employee> employees = new ArrayList<Employee>();
		employees.add(new Employee(1, "Venkat"));
		employees.add(new Employee(2, "Saisree"));
		employees.add(new Employee(3, "Rushi"));
		employees.add(new Employee(4,"Venkat"));
		
		employees.stream().filter((arg)->arg.getEmpName()=="Venkat").forEach(val->{
			System.out.println(val.getEmpId());
			System.out.println(val.getEmpName());
		});

}
}
