/**
 * 
 * @author ks.venkatsai
 *
 */
public class test {

	public static void main(String[] args) {
		
		//Print the name to console using iteration
		Sample obj = new Sample();
		System.out.println("Is the name printed to console:"+obj.loop("Venkatsai",true));
		
		//Print the sum of 2 num
		add obj1 = new add();
		System.out.println(obj1.addition(1,2));
		
		//void, return a int,return other than int
		test2 obj2 = new test2();
		obj2.fun();
		System.out.println(obj2.func());
		System.out.println(obj2.funct(1));
		System.out.println(obj2.functi(1));
		
		//obj as parameter
		test3 obj3 = new test3();
		obj3.functio(obj3);
		System.out.println("D and E values are:"+obj3.d+","+obj3.e);
		
		test4 obj4 = new test4();
		obj4.Empid = 1;
		obj4.EmpSal = 25000;
		obj4.EmpName = "xyz";
		obj4.display();
		
		test4 obj5 = new test4();
		obj5.Empid = 2;
		obj5.EmpSal = 30000;
		obj5.EmpName = "abc";
		obj5.display();
		
		
}
}