package com.main;

import java.util.ArrayList;
import java.util.List;

import com.model.Employee;

public class ForEach {

	public static void main(String[] args) {
		Employee employee = new Employee(1, "Sai");
		Employee employee1 = new Employee(2, "Saisree");
		Employee employee2 = new Employee(3, "Venkat");
		
		List<Employee> employees = new ArrayList<Employee>();
		employees.add(employee);
		employees.add(employee1);
		employees.add(employee2);
		
		employees.forEach((emp)->{
			System.out.println(emp.getEmpId());
			System.out.println(emp.getEmpName());
		});
	}

}
