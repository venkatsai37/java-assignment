package com.main;

import com.service.FunctionalProg;

public class FuncProgMain {

	public static void main(String[] args) {
		FunctionalProg functionalProg = new FunctionalProg();
		String string = functionalProg.function((var)->{ return "Hello" ;} );
		System.out.println(string);

	}
 
}
