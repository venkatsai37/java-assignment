package com.main;

import java.sql.Connection;

import com.dao.MySqlConnection;
import com.dao.Userdao;
import com.dao.UserdaoImpl;
import com.exception.LoginException;
import com.model.UserModel;

/**
 * 
 * @author ks.venkatsai
 *
 */
public class MainJdbc {

	public static void main(String[] args) {
		Connection connection = MySqlConnection.getConnection();
		Userdao userdao = new UserdaoImpl();
		UserModel user = new UserModel();
		user.setUserName("venkat");
		try {
			UserModel model = userdao.deleteUser(user);
			System.out.println("User deleted");
		} catch (LoginException e) {
			System.err.println(e.getMessage());
		}
		/*user.setPassword("saisree123");
		user.setUserName("sai");
		try {
			UserModel model = userdao.updateUser(user);
			System.out.println("User info saved "+model.getPassword());
		} catch (LoginException e) {
			System.err.println(e.getMessage());
		}*/
		/*try {
			UserModel user = userdao.readUser(2);
			if (user != null) {
				System.out.println("Hi:"+user.getUserName());
			} else {

			}
		} catch (LoginException e) {
			System.err.println(e.getMessage());
		}*/
		/*
		 * UserModel user = new UserModel(); user.setUserName("Harsha");
		 * user.setPassword("Gudodu"); try { UserModel model = userdao.createUser(user);
		 * System.out.println("User info saved "+model.getUserName()); } catch
		 * (LoginException e) { System.err.println(e.getMessage()); }
		 */
	}

}
