package com.main;

import com.model.Address;
import com.model.Department;
import com.model.Employee;
/**
 * 
 * @author ks.venkatsai
 *
 */
public class Main {

	public static void main(String[] args) {
		Employee employee1 = new Employee(1, "xyz", 21000f, 21);
		Employee employee2 = new Employee(2, "abc", 22000f, 22);
		Employee[] employees = new Employee[2];
		employees[0] = employee1;
		employees[1] = employee2;

		Department department = new Department(1, "CSE");
		department.setEmployee(employees);

		System.out.println("Depart No:" + department.getDeptNo());
		System.out.println("Depart Name:" + department.getDeptName());
		Employee[] arry = department.getEmployee();
		for (int i = 0; i < arry.length; i++) {
			System.out.println("EmpNo:" + arry[i].getEmpNO());
			System.out.println("EmpName:" + arry[i].getEmpName());
			System.out.println("Empsalary:" + arry[i].getEmpSalary());
			System.out.println("EmpAge:" + arry[i].getEmpAge());
		}

		Department department2 = new Department(2, "ECE");
		Employee[] employees2 = new Employee[2];
		employees2[0] = employee1;
		employees2[1] = employee2;

		System.out.println("Depart No:" + department2.getDeptNo());
		System.out.println("Depart Name:" + department2.getDeptName());
		Employee[] employees3 = department2.getEmployee();
		for (int i = 0; i < employees2.length; i++) {
			System.out.println("EmpNo:" + employees2[i].getEmpNO());
			System.out.println("EmpName:" + employees2[i].getEmpName());
			System.out.println("Empsalary:" + employees2[i].getEmpSalary());
			System.out.println("EmpAge:" + employees2[i].getEmpAge());
			
		Address address = new Address(1,"Chittoor","AP",517001);
		Address address2 = new Address(2,"Bengalore","KA",517002);
		Address[] addresses = new Address[2];
		addresses[0] = address;
		addresses[1] = address2;
		
		employee1.setAddresses(addresses);
		employee2.setAddresses(addresses);
		
		System.out.println("Empno:" +employee1.getEmpNO());
		System.out.println("Empname"+employee1.getEmpAge());
		Address[] addresses2 = employee1.getAddresses();
		for (int j = 0; j < addresses2.length; j++) {
			System.out.println("Add doorno:"+addresses2[i].getDoorno());
			System.out.println("Add State:"+addresses2[i].getState());
		}
		}

	}

}
