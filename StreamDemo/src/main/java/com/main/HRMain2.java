package com.main;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import com.model.Employee;

public class HRMain2 {

	public static void main(String[] args) {
		Employee employee1 = new Employee(1, "Sai", 'A', 20000.10f);
		Employee employee2 = new Employee(2, "Saisree", 'E', 30000.10f);
		Employee employee3 = new Employee(3, "Rushi", 'D', 40000.10f);
		Employee employee4 = new Employee(4, "Harsha", 'E', 50000.10f);
		
		List<Employee> employees = new ArrayList<Employee>();
		employees.add(employee1);
		employees.add(employee2);
		employees.add(employee3);
		employees.add(employee4);
		
		List<Float> col = employees.stream().filter((arg) -> arg.getBand()=='E').map((input) -> input.getSalary() +100).collect(Collectors.toList());
		System.out.println(col.size());
		
		// As stream doesnt store so I am using collectors and storing it in list and displaying
		
		
		/*forEach((emp)->{
			System.out.println("Emp no:"+emp.getEmpNo());
			System.out.println("Emp Name:"+emp.getEmpName());
			System.out.println("Emp Band:"+emp.getBand());
			System.out.println("Emp sal:"+emp.getSalary());
		});*/
		}

}

