package com.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.exception.LoginException;
import com.model.UserModel;

public class UserdaoImpl implements Userdao {

	@Override
	public UserModel createUser(UserModel user) throws LoginException {
		Connection connection = MySqlConnection.getConnection();
		String sql = "insert into user(user_name,password) values(?,?)";
		try {
			PreparedStatement preparedstatement = connection.prepareStatement(sql);
			preparedstatement.setString(1, user.getUserName());
			preparedstatement.setString(2, user.getPassword());
			int checkInstertion = preparedstatement.executeUpdate();
			if (checkInstertion > 0) {
				System.out.println("Insertion is successfull");
			} else {
				System.out.println("Insertion failed");
			}
			connection.close();
		} catch (SQLException e) {
			throw new LoginException("Cannot Insert");
		}
		return user;
	}

	@Override
	public UserModel readUser(int sapid) throws LoginException {
		UserModel user = null;
		Connection connection = MySqlConnection.getConnection();
		String sql = "Select * from user where user_id = ?";
		try {
			PreparedStatement preparedstatement = connection.prepareStatement(sql);
			preparedstatement.setInt(1, sapid);
			ResultSet resultSet = preparedstatement.executeQuery();
			user = new UserModel();
			while (resultSet.next()) {
				/*
				 * System.out.println(resultSet.getInt("user_id"));
				 * System.out.println(resultSet.getInt("user_name"));
				 * System.out.println(resultSet.getInt("password"));
				 */

				user.setSapid(resultSet.getInt("user_id"));
				user.setUserName(resultSet.getString("user_name"));
				user.setUserName(resultSet.getString("password"));
			}
		} catch (SQLException e) {
			throw new LoginException("Cant fetch details");
		}
		return user;
	}

	@Override
	public UserModel updateUser(UserModel user) throws LoginException {
		Connection connection = MySqlConnection.getConnection();
		String sql = "update user set password=? where user_name=?";
		try {
			PreparedStatement preparedStatement = connection.prepareStatement(sql);
			preparedStatement.setString(1,user.getPassword());
			preparedStatement.setString(2,user.getUserName());
			int checkInstertion = preparedStatement.executeUpdate();
			if (checkInstertion > 0) {
				System.out.println("Insertion is successfull");
			} else {
				System.out.println("Insertion failed");
			}
			connection.close();
			
		} catch (SQLException e) {
			throw new LoginException("Cannot Insert");
		}
		return user;
	}

	@Override
	public UserModel deleteUser(UserModel user) throws LoginException {
		Connection connection = MySqlConnection.getConnection();
		String sql = "delete from user where user_name=?";
		try {
			PreparedStatement preparedStatement = connection.prepareStatement(sql);
			preparedStatement.setString(1, user.getUserName());
			int checkdeletion = preparedStatement.executeUpdate();
			if (checkdeletion > 0) {
				System.out.println("deletion is successfull");
			} else {
				System.out.println("deletion failed");
			}
			connection.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

}
