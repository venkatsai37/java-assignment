package com.main;


import com.model.Employee;
import com.service.SingleAbsInf3;

public class LamdaExprWithArgAndParObj {

	public static void main(String[] args) {
		SingleAbsInf3 singleAbsInf3 = (emp) -> {
			emp.setEmpName(emp.getEmpName() + "Welcome to HCL!!");
			return emp;
		};
		Employee employee = singleAbsInf3.run(new Employee(1, "Venkat ") );
		System.out.println(employee.getEmpId());
		System.out.println(employee.getEmpName());
	}

}
