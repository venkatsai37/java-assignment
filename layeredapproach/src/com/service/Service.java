package com.service;

public class Service {
	public int sumAllValues(int[] var) {
		int sample = 0;
		for (int i = 0; i < var.length; i++) {
			sample += var[i];
		}
		return sample;
	}

	public int findBiggest(int[] var) {
		int sample = 0;
		for (int i = 0; i < var.length; i++) {
			if (var[i] > sample) {
				sample = var[i];
			}
		}
		return sample;
	}

	public boolean searchElement(int[] var) {
		int sample = 0;
		for (int i = 0; i < var.length; i++) {
			if (var[i] == 2) {
				sample = var[i];
			}
		}
		if (sample > 0) {
			return true;

		} else {
			return false;
		}
	}
}
