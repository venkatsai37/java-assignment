package com.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

//Mapping to DB

@Entity
@Table(name = "StudentTable")
@NamedQueries(  
	    {  
	        @NamedQuery(  
	        name = "findStudentFByName",  
	        query = "from Student as stud where stud.firstName = :fname"  
	        )  ,
	   
	        @NamedQuery(  
	        name = "findStudentLByName",  
	        query = "from Student as stud where stud.lastName = :lname"  
	        )  
	    }  
	)  

public class Student implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "StudentID")
	private int StudId;
	@Column(name = "first_Name", length = 25)
	private String firstName;
	@Column(name = "last_Name", length = 25)
	private String lastName;
	@Column(name = "MailId", length = 25)
	private String emailId;

	public Student() {
		super();
	}

	public Student(String firstName, String lastName, String emailId) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.emailId = emailId;
	}

	public int getStudId() {
		return StudId;
	}

	public void setStudId(int studId) {
		StudId = studId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

}
