package com.main;

import com.service.SingleAbsInf2;

public class LamdaExprWithArgAndPar {

	public static void main(String[] args) {
		SingleAbsInf2 singleAbsInf2 = (arg) -> {
			return "Welcome " + arg;
		};
		System.out.println(singleAbsInf2.run("Venkat"));
	}

}
