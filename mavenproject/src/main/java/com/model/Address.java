package com.model;

public class Address {
	private int doorno;
	private String city;
	private String state;
	private int zipcode;
	
	public Address() {
		super();
	}
	public Address(int doorno, String city, String state, int zipcode) {
		super();
		this.doorno = doorno;
		this.city = city;
		this.state = state;
		this.zipcode = zipcode;
	}
	public int getDoorno() {
		return doorno;
	}
	public void setDoorno(int doorno) {
		this.doorno = doorno;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public int getZipcode() {
		return zipcode;
	}
	public void setZipcode(int zipcode) {
		this.zipcode = zipcode;
	}
	
	
}
