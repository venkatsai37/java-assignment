package com.service;

public interface CalculatorService {
	public abstract int ADD(int a, int b);

	public abstract int SUB(int a, int b);

	public abstract int MUL(int a, int b);

	public abstract int DIV(int a, int b);

	public abstract double sin(double formated);

	public abstract double cos(double number);

	public abstract double tan(double number);
}
