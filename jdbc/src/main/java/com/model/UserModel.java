package com.model;

import java.io.Serializable;

public class UserModel implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int sapid;
	private String userName;
	private String password;
	public UserModel() {
		super();
	}
	public UserModel(int sapid, String userName, String password) {
		super();
		this.sapid = sapid;
		this.userName = userName;
		this.password = password;
	}
	public int getSapid() {
		return sapid;
	}
	public void setSapid(int sapid) {
		this.sapid = sapid;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
}
