package com.main;

import com.service.Service;

public class Array {

	public static void main(String[] args) {
		int[] var = {1,2,3,4};
		
		Service ser = new Service();
		int sum = ser.sumAllValues(var);
		System.out.println("The total sum of array elements is :"+sum);
		
		Service serv = new Service();
		int Biggest = serv.findBiggest(var);
		System.out.println("The Biggest element in the Array element is:"+Biggest);
		
		Service serve = new Service();
		boolean search = serve.searchElement(var);
		System.out.println("Searching 2 element in the Array element:"+search);
	}

}
