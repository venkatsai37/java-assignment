package com.main;

import com.service.SingleAbsInf;

public class LamdaExprWithoutPar {

	public static void main(String[] args) {
		SingleAbsInf singleAbsInf = ( ) -> System.out.println("I am using lamda exp for FunctionalInterface method run()");
		singleAbsInf.run();
		singleAbsInf.defaultMethod();
		SingleAbsInf.staticMethod();
		
		

	}

}
