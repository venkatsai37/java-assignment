package com.main;

import com.service.MedRefClass;
import com.service.MedRefInf;

public class MethodRef {

	public static void main(String[] args) {
		MedRefClass medRefClass = new MedRefClass();
		
		MedRefInf medRefInf = medRefClass::function;
		
		medRefInf.run();

	}

}
