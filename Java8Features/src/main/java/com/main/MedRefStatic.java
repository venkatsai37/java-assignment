package com.main;

import java.util.function.BiFunction;

import com.service.StaticInterface;

public class MedRefStatic {

	public static void main(String[] args) {
		BiFunction<Integer, Integer, Integer> biFunction = StaticInterface::add;
		System.out.println(biFunction.apply(1, 2));
	}

}
