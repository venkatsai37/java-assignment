package com.model;

public class Employee {
	private int empNo;
	private String empName;
	private char band;
	private float salary;
	public Employee() {
		super();
	}
	public Employee(int empNo, String empName, char band, float salary) {
		super();
		this.empNo = empNo;
		this.empName = empName;
		this.band = band;
		this.salary = salary;
	}
	public int getEmpNo() {
		return empNo;
	}
	public void setEmpNo(int empNo) {
		this.empNo = empNo;
	}
	public String getEmpName() {
		return empName;
	}
	public void setEmpName(String empName) {
		this.empName = empName;
	}
	public char getBand() {
		return band;
	}
	public void setBand(char band) {
		this.band = band;
	}
	public float getSalary() {
		return salary;
	}
	public void setSalary(float salary) {
		this.salary = salary;
	}
	
	
	
}
