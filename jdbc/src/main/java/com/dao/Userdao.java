package com.dao;

import com.exception.LoginException;
import com.model.UserModel;

public interface Userdao {
	public abstract UserModel createUser(UserModel user) throws LoginException;
	public abstract UserModel readUser(int sapid) throws LoginException;
	public abstract UserModel updateUser(UserModel user) throws LoginException;
	public abstract UserModel deleteUser(UserModel user) throws LoginException;
}
