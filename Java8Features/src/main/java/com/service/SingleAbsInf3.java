package com.service;

import com.model.Employee;

/**
 * This is to demonstrate Functional programming
 * @author ks.venkatsai
 *
 */
@FunctionalInterface
public interface SingleAbsInf3 {
	public abstract Employee run(Employee employee);
	
	public default void defaultMethod() {
		System.out.println("Its a default Method");
	}
	
	public static void staticMethod() {
		System.out.println("Its a static method");
	}
}
