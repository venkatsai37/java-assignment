package com.dao;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.query.Query;

import com.model.Employee;

/**
 * This is to demostrate Criteria in hibernate
 * 
 * @author ks.venkatsai
 *
 */
public class HibernateCritiria {
	public static void main(String[] args) {
		StandardServiceRegistry ssr = new StandardServiceRegistryBuilder().configure("hibernate.xml")
		.build();
		Metadata meta = new MetadataSources(ssr).getMetadataBuilder().build();
		SessionFactory factory = meta.getSessionFactoryBuilder().build();
		Session session = factory.openSession();

		javax.persistence.criteria.CriteriaQuery<Employee> cq = session.getCriteriaBuilder().createQuery(Employee.class);
		cq.from(Employee.class);
		session.createQuery(cq).getResultList();
		CriteriaBuilder builder = session.getCriteriaBuilder();
		javax.persistence.criteria.CriteriaQuery<Employee> query = builder.createQuery(Employee.class);

		Root<Employee> root = (Root) query.from(Employee.class);;
		//query.select(root.get("employeeName"));
		Query<Employee> query1=session.createQuery(query);
		List<Employee> employees = query1.getResultList();
		for (Employee employee : employees) {
		System.out.println(employee.getEmpNo());
		System.out.println(employee.getEmpName());
		}

		session.close();
		factory.close();
		}
		}