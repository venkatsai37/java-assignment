package com.service;

import com.model.Employee;
@FunctionalInterface
public interface MedRefConsInf {
	public abstract Employee run(int a, String str);
}
