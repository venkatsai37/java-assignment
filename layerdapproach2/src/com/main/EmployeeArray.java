package com.main;

import java.util.Scanner;

import com.model.Employee;
import com.service.EmployeeService;
/**
 * 
 * @author ks.venkatsai
 *
 */
public class EmployeeArray {

	

	public static void main(String[] args) {
		Employee employee1 = new Employee(1, "xyz", 21, 24000f);
		Employee employee2 = new Employee(2, "abc", 22, 25000f);
		Employee employee3 = new Employee(3, "sai", 23, 26000f);
		Employee employee4 = new Employee(4, "venkat", 24, 27000f);

		Employee[] employees = new Employee[4];
		employees[0] = employee1;
		employees[1] = employee2;
		employees[2] = employee3;
		employees[3] = employee4;

		EmployeeService employeeService = new EmployeeService();
		Scanner scan = new Scanner(System.in);
		System.out.println("Enter id to search");
		int sc = scan.nextInt();
		Employee employee = employeeService.searchById(employees, sc);
		if (employee != null) {
			System.out.println("Emp number:" + employee.getEmpid());
			System.out.println("Emp name:" + employee.getEmpname());
		} else {
			System.out.println("Id is not associated with anyother records");
		}
		System.out.println("Enter age to search");
		int ans = scan.nextInt();
		Employee employeee = employeeService.searchByAge(employees, ans);
		if (employeee != null) {
			System.out.println("Emp name:" + employee.getEmpname());
		}
		else {
			System.out.println("Id is not associated with anyother records");
		}
		scan.close();
	}

}
